## Práctica final Curso JavaScript

*Alumno: Alí Rodríguez 

Este proyecto consta en una página web que nos permite ver un catalogo de películas, en dicho catalogo podemos ver el póster de la misma, el titulo, calificación,
sinopsis, etc...

Tiene varias otras páginas para mostrar más películas, y tiene una página donde se puede buscar películas de acuerdo a una palabra.

Era esta sección la que teníamos que codificar para que funcionara. Lo que yo hice fue usar la url de la función de búsqueda de la API que se usó para el proyecto.
A partir de ahí, los elementos se iban a guardar en una lista, donde cada elemento se iba a guardar en una etiqueta <li>, entiéndase por elemento a el nombre de cada
película que coincidiera con la búsqueda.
Así, cuando la lista se mostrara en el archivo HTML se iban a poder visualizar todas las películas que coincidieran con la búsqueda.

Eso fue lo que intente hacer, sin embargo creo que tuve algún error, intente arreglarlo de todas las formas posibles, pero no entendí como hacer funcionar 
esta utilidad de la página. No hay instrucciones suficientes para ello, intente buscar soluciones en internet pero no encontré nada que se adaptara a esto.

